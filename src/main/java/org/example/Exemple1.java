package org.example;

import java.util.Scanner;

public class Exemple1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        rezolvareProb1(x);
    }


    static void rezolvareProb1(int n) {
        // calculați suma tutor cifrelor lui n
        // câte cifre are n
        // câte cifre pare are n
        // câte cifre impare are n

        int count = 0;
        int numereCifre = 0;
        int nrPare = 0;
        int nrImpare = 0;
        System.out.print("Suma numerelor lui " + n + " este:");
        while (n > 0) {
            count = count + n % 10;
            n /= 10;
            if (count % 2 == 0) {
                nrPare++;
            } else {
                nrImpare++;
            }
            numereCifre++;
        }
        System.out.println(" " + count + " si are " + numereCifre + " cifre!");
        System.out.println("Sunt " + nrPare + " numar/numere pare si " + nrImpare + " numar/numere impare");

    }
}
