package org.example;

import java.util.Scanner;

public class Exercitiul2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        rezolvareProb2(n);

    }

    static void rezolvareProb2(int n) {
        // afisati toti divizorii lui n citit de la tastatura
        // suma divizorilor lui n
        // cati dvizori are n
        // cati divizori propri are n (divizor propriu = diferit de 1 si el insusi)

        int contDiv = 0;
        int sumDiv = 0;
        int countDivProp = 0;

        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                sumDiv += i;
                contDiv++;
                System.out.print(i + " ");
                if (i == 1 || i == n) {
                    countDivProp++;
                }
            }
        }
        System.out.println("");
        System.out.println("Suma divizorilor lui " + n + " este: " + sumDiv);
        System.out.println( "are: " + contDiv + " divizori, " + (contDiv - countDivProp) + " divizori proprii");


    }
}
